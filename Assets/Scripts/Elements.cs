﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elements {

    public static Dictionary<string, Element> elements = new Dictionary<string, Element>() {
        {"Fire", new Element("Fire", new List<Effect>() { Effect.WeakToWater, Effect.FireResistant }, new List<Attribute>() { Attribute.DamageUp }, ShotType.SpreadShot)}
    };
}
