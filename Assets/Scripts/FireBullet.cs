﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : Bullet {
    public float base_Damage = .05f;
    public float movement_Speed = .2f;

    public override IElement element { get; set; }
    public override float baseDamage { get; set; }
    public override float movementSpeed { get; set; }

    // Use this for initialization
    void Start () {
        element = Elements.elements["Fire"];
        baseDamage = base_Damage;
        movementSpeed = movement_Speed;
	}

    void Update()
    {
        transform.position += Vector3.forward * movement_Speed * Time.deltaTime;
    }
}
