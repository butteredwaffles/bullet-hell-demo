﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Bullet : MonoBehaviour
{

    private float damageDownModifier = .5f;
    private float damageUpModifier = .7f;

    public abstract IElement element { get; set; }
    public abstract float baseDamage { get; set; }
    public abstract float movementSpeed { get; set; }

    protected float AdjustForAttributes()
    {
        float newDamage = baseDamage;
        foreach (Attribute attr in element.attributes)
        {
            switch(attr)
            {
                case Attribute.DamageDown:
                    newDamage -= damageDownModifier;
                    break;
                case Attribute.DamageUp:
                    newDamage += damageUpModifier;
                    break;                
            }
        }
        return newDamage;
    }
}
