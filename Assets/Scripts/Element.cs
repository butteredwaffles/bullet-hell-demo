﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IElement {
    string name { get; set; }
	List<Effect> effects { get; set; }
    List<Attribute> attributes { get; set; }
    ShotType shotType { get; set; }
}

public class Element : IElement
{
    public string name { get; set; }
    public List<Effect> effects { get; set; }
    public List<Attribute> attributes { get; set; }
    public ShotType shotType { get; set; }

    public Element(string nme, List<Effect> effs, List<Attribute> attrs, ShotType stype)
    {
        name = nme;
        effects = effs;
        attributes = attrs;
        shotType = stype;
    }
}

public enum Effect {
    None,
    WeakToWater,
    WeakToFire,
    WeakToEarth,
    WeakToAir,
    WaterResistant,
    FireResistant,
    EarthResistant,
    AirResistant,
}


public enum ShotType {
    SpreadShot,
    FocusedShot
}


public enum Attribute {
    None,
    Fragile,
    DamageDown,
    DamageUp
}
