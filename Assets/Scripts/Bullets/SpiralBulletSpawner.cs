﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiralBulletSpawner : MonoBehaviour {
    public GameObject bullet; // the bullet that this spawner makes
    public float interval = .5f; // the interval at which the bullets appear
    public int numberOfBullets = 100; // pretty self-explanatory


	// Use this for initialization
	void Start () {
        var currAngle = 0f;
        for (int i = 0; i < numberOfBullets; i++)
        {
            Vector3 newDir = Vector3.RotateTowards(transform.forward, transform.position, 1.0f * Time.deltaTime, 0.0f);
            float x = (transform.position.x * Mathf.Cos(currAngle) * 360) * Time.deltaTime;
            float y = (transform.position.y * Mathf.Sin(currAngle) * 360) * Time.deltaTime;
            //GameObject newBullet = Instantiate(bullet, new Vector3(x, y), Quaternion.LookRotation(newDir));
            Instantiate(bullet, transform.position + new Vector3(x, y), Quaternion.LookRotation(newDir));
            currAngle += 5f;
        }
    }
	
	// Update is called once per frame
	void Update () {
        
	}
}
